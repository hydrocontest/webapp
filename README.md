# Boatlink / Webapp

This is the web app for the boat telemetry system used by the HEIA-FR team for Hydrocontest. It uses Vuejs and Nuxt and is behind a Nginx reversed proxy.
