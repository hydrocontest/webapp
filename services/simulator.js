export default (vm) => {
  setInterval(function(){
    vm.$store.commit('boat/set', {
      batteryPercent: Math.round(Math.random()*100),
      boatSpeed: Math.round(Math.random()*30),
      batteryCurrent: Math.round(Math.random()*60),
    })
    vm.$store.commit('box/set', {
      temperature: 10 + Math.round(Math.random()*80),
      humidity: 30 + Math.round(Math.random()*40),
    })

    vm.$store.commit('watchdog/resetWifi')
    vm.$store.commit('watchdog/resetBluetooth')

  }, 1000)
}
