import io from 'socket.io-client'

export default (vm) => {

  const parsedUrl = new URL(window.location.href)
  const sp = parsedUrl.searchParams

  var socketUrl = null
  if (sp.has('io')) {
    socketUrl = new URL(sp.get('io'))
  } else if (parsedUrl.hostname == '127.0.0.1' || parsedUrl.hostname == 'localhost') {
    socketUrl = new URL('http://' + parsedUrl.hostname + ':5000')
  }
  const socket = (socketUrl === null) ? io() : io(socketUrl.href)

  socket.on('message', function(msg) {
    if (msg.msgType == 'from-boat') {
      vm.$store.commit('boat/set', msg)
      vm.$store.commit('watchdog/resetBluetooth')
      vm.$store.commit('watchdog/resetWifi')

    } else if (msg.msgType == 'from-box') {
      vm.$store.commit('box/set', {
        temperature: msg.t,
        humidity: msg.h
      })
      vm.$store.commit('watchdog/resetWifi')

    }
  })

  socket.on('connect_error', function() {
    vm.$store.commit('watchdog/fail')
  })

  socket.on('connect', () => {
    vm.$store.commit('watchdog/resetWifi')
  })

}
