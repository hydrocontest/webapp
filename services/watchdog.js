export default (vm) => {
  setInterval(function(){
    vm.$store.commit('watchdog/tick')
  }, 500)
}
