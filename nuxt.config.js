const webpack = require('webpack')

module.exports = {
  /*
  ** Headers of the page
  */
  mode: 'spa',
  head: {
    title: 'Boatlink',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Boatlink for Hydrocontest' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/logo-192.png' },
      { rel: 'manifest', href: '/manifest.json' }
    ]
  },
  modules: [
    ['bootstrap-vue/nuxt', { css: false }],
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#007CB7' },
  /*
  ** Build configuration
  */
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        '$': 'jquery',
        // '_': 'lodash'
      })
    ],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
