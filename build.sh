#!/bin/bash

VERSION=$(cat package.json | jq -r .version)
BASE="boatlink-webapp"
TARGET="${BASE}-${VERSION}"
DEBVERSION=${VERSION}"-0"
DEBTARGET="${BASE}-${DEBVERSION}"

# Cleanup
rm -Rf $TARGET
rm *.tar.gz
rm *.deb

# Build
npm run build

# Build tar.gz
cp -a ./dist $TARGET
tar czvf ${TARGET}.tar.gz ${TARGET}
rm -Rf $TARGET

# Build deb
rm -Rf ${DEBTARGET}
mkdir -p ${DEBTARGET}/srv/www
cp -a dist ${DEBTARGET}/srv/www/boatlink
mkdir ${DEBTARGET}/DEBIAN
echo "{\"version\":\"${DEBVERSION}\"}" | npx mustache - DEBIAN/control > ${DEBTARGET}/DEBIAN/control
dpkg-deb --build ${DEBTARGET}
rm -Rf ${DEBTARGET}
