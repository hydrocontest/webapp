import VuexPersistence from 'vuex-persist'

export const strict = false

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  modules: ['settings']
})

export const state = () => ({})
export const mutations = {}
export const plugins = [vuexLocal.plugin]
