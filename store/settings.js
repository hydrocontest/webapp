export const state = () => ({
  speedUnit: 'kmh',
})

export const mutations = {
  setKmh (state) {
    state.speedUnit = 'kmh'
  },
  setKts (state) {
    state.speedUnit = 'kts'
  },
}
