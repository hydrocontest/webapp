export const state = () => ({
  data: {
    temperature: 0,
    humidity: 0,
  }
})

export const mutations = {
  set (state, payload) {
    Object.keys(state.data).forEach(v => {
      if (payload.hasOwnProperty(v)) {
        state.data[v] = payload[v]
      }
    })
  }
}
