export const state = () => ({
  data: {
    boatSpeed: 0,
    boatSpeedK: 0,
    motorSpeed: 0,
    batteryCurrent: 0,
    batteryPercent: 0,
    batteryVoltage: 0,
    leftFoil: 0,
    rightFoil: 0,
    temperature1: 0,
    batteryMaxCurrent: 0,
    ppmDriver: 0,
    error1: 0,
    error2: 0,
  },
  historical: []
})

const kts = 1.852
const histLen = 60

export const mutations = {
  set (state, payload) {

    Object.keys(state.data).forEach(v => {
      if (payload.hasOwnProperty(v)) {
        state.data[v] = payload[v]
      } else {
        state.data[v] = 0
      }
    })
    if (payload.boatSpeed) {
      state.data.boatSpeedK = Math.round(payload.boatSpeed * 10 / kts) / 10
    }

    state.historical.push({
      label: new Date().toLocaleTimeString(),
      data: payload
    })
    if (state.historical.length > histLen) {
      state.historical = state.historical.slice(state.historical.length - histLen)
    }
  }
}
