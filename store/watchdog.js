export const state = () => ({
  wifi: 0,
  bluetooth: 0,
})

export const mutations = {
  tick (state) {
    state.wifi++
    state.bluetooth++
  },
  fail (state) {
    state.wifi = 1000
    state.bluetooth = 1000
  },

  resetWifi (state) {
    state.wifi = 0
  },
  resetBluetooth (state) {
    state.bluetooth = 0
  },
}
